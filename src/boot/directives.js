import Vue from 'vue';

Vue.directive('number-only', {
  bind(el) {
    el.addEventListener('keydown', (e) => {
      if (
        [
          'Digit0',
          'Digit1',
          'Digit2',
          'Digit3',
          'Digit4',
          'Digit5',
          'Digit6',
          'Digit7',
          'Digit8',
          'Digit9',
          'Escape',
          'Tab',
          'Backspace',
          'Enter',
          'Delete',
          'Home',
          'End',
          'ArrowRight',
          'ArrowLeft',
        ].indexOf(e.code) !== -1
              // Allow: Ctrl+A
              || (e.code === 'KeyA' && e.ctrlKey === true)
              // Allow: Ctrl+C
              || (e.code === 'KeyC' && e.ctrlKey === true)
              // Allow: Ctrl+X
              || (e.code === 'KeyX' && e.ctrlKey === true)
      ) {
        return;
      }
      e.preventDefault();
    });
  },
});
