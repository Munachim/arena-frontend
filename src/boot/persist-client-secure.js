import createPersistedState from 'vuex-persistedstate';
import SecureLS from 'secure-ls';

const ls = new SecureLS({ isCompression: false });

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({ store }) => {
  window.setTimeout(() => {
    createPersistedState({
      storage: {
        getItem: (key) => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: (key) => ls.remove(key),
      },
    })(store);
  }, 0);
};
