import store from 'src/store/index';
import { getObject } from '../helpers/localStorage';

const audienceProfile = getObject('audienceProfile');

if (audienceProfile) {
  store().commit('audience/SET_AUDIENCE_PROFILE', audienceProfile);
}
