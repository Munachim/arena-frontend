// import something here
import SocialMediaShare from 'vue-social-sharing';
// "async" is optional
export default async ({ Vue }) => {
  Vue.use(SocialMediaShare);
};
