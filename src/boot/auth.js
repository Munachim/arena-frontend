import { authClient } from 'src/config/client.js';

export default ({ store }) => {
  // if user is still logged in but audienceProfile doesn't exist in the store
  if (!store.state.audience.audienceProfile.id && store.state.auth.isAuthenticated) {
    store.dispatch('audience/getAudienceProfile');
  }
  authClient.interceptors.response.use(
    (response) => response,
    (error) => {
      if (error.response.status === 401) {
        store.commit('auth/SIGN_OUT');
      }
      return Promise.reject(error);
    },
  );
};
