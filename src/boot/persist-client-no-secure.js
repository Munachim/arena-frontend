import createPersistedState from 'vuex-persistedstate';
// import { LocalStorage } from 'quasar';

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({ store }) => {
  window.setTimeout(() => {
    createPersistedState({
      storage: {
        getItem: (key) => localStorage.getItem(key),
        removeItem: (key) => localStorage.removeItem(key),
        setItem: (key, value) => localStorage.setItem(key, value),
      },
    })(store);
  }, 0);
};
