import Vue from 'vue';
import axios from 'axios';

Vue.prototype.$axios = axios.create({
  baseURL: process.env.BASE_URL,
  timeout: 50000,
});
