// DEPENDENCIES
/**
 * Phone Number International format
 * @param phone
 * @returns {string}
 */
module.exports.formatPhoneNumber = (phone) => `+234${phone.substring(1, 11)}`;
