import { saveItem } from 'src/helpers/localStorage';

const saveStatePlugin = (store) => {
  store.subscribe((mutation) => {
    if (mutation.type === 'auth/SET_PHONE_NUMBER') {
      saveItem('phoneNumber', mutation.payload);
    }
    if (mutation.type === 'auth/SET_PIN_ID') {
      saveItem('pinId', mutation.payload);
    }
    if (mutation.type === 'auth/SET_SIGN_UP_DETAILS') {
      saveItem('signupDetails', mutation.payload);
    }
    if (mutation.type === 'auth/SET_IS_USER_ALREADY_REGISTERED') {
      saveItem('isUserAlreadyRegistered', mutation.payload);
    }
    if (mutation.type === 'auth/SET_IS_USER_AUTHENTICATED') {
      saveItem('isAuthenticated', mutation.payload);
    }
    if (mutation.type === 'auth/SET_ACCESS_TOKEN') {
      saveItem('accessToken', mutation.payload);
    }
    if (mutation.type === 'auth/SET_USER_ID') {
      saveItem('userId', mutation.payload);
    }
    if (mutation.type === 'audience/SET_AUDIENCE') {
      saveItem('audience', mutation.payload);
    }
    if (mutation.type === 'audience/SET_AUDIENCE_PROFILE') {
      saveItem('audienceProfile', mutation.payload);
    }
  });
};

export default saveStatePlugin;
