const getObject = (name) => {
  const queryResponse = localStorage.getItem(name);
  if (queryResponse) {
    return JSON.parse(queryResponse);
  }
  return null;
};

const saveItem = (name, payload) => {
  if (payload) {
    return localStorage.setItem(name, JSON.stringify(payload));
  }
  return false;
};

const clearAll = () => localStorage.clear();

const removeItem = (name) => localStorage.removeItem(name);

const getItem = (name) => JSON.parse(localStorage.getItem(name));

export {
  getObject,
  getItem,
  saveItem,
  clearAll,
  removeItem,
};
