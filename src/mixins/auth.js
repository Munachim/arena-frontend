import { mapState } from 'vuex';
import Notify from 'src/config/notify';
import { getItem } from 'src/helpers/localStorage';

const authMixin = {
  methods: {
    async dispatchLoginAPI(payload) {
      await this.$store.dispatch('auth/signIn', payload);
    },
    async loginUser(pin) {
      try {
        const payload = {
          phoneNumber: getItem('phoneNumber'),
          pin,
        };
        this.$q.loading.show();
        await this.dispatchLoginAPI(payload);
        this.$q.loading.hide();

        if (this.loginUserSuccess) {
          this.$router.replace({ name: 'Isabisport GameRules' });
        } else {
          Notify.error(this.loginUserErrorMessage);
        }
      } catch (error) {
        Notify.error('There was an error signing in. Please try again later');
        this.$q.loading.hide();
      }
    },
    async dispatchValidateOtpApi(payload) {
      await this.$store.dispatch('auth/validateOtp', payload);
    },
    logOut() {
      const toInfluencerAuth = localStorage.getItem('influencer_username') || false;

      this.$store.dispatch('auth/signOut')
        .then(() => {
          if (toInfluencerAuth) {
            this.$router.replace({ name: 'one-audience', params: { slug: toInfluencerAuth } });
          } else {
            this.$router.replace({ name: 'Topbrain Login' });
          }
        });
    },
  },
  computed: {
    ...mapState('auth', [
      'loginUserSuccess',
      'loginUserErrorMessage',
      'isUserAlreadyRegistered',
    ]),
  },
};

export default authMixin;
