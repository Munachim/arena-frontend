import { copyToClipboard } from 'quasar';

export const utilsMixin = {
  methods: {
    copy(value) {
      return copyToClipboard(value);
    },
    isValidDigit(val) {
      const reg = /^\d+$/;
      return val.match(reg);
    },
    shouldFocusNext(val, dataName) {
      const currentRef = this.$refs[val - 1];
      if (Number.isNaN(Number(currentRef.value))) {
        this[dataName] = null;
        return false;
      }
      if (this[dataName] === null || this[dataName].trim() === '') {
        this[dataName] = null;
        return false;
      }
      if (!Number.isNaN(Number(currentRef.value))) {
        this.$refs[val].focus();
        return true;
      }
      return false;
    },
    shouldFocusPrev(val, dataName) {
      const nums = ['one', 'two', 'three', 'four', 'five', 'six'];
      const prevRef = this.$refs[val - 2];
      this[dataName] = null;
      this[nums[nums.indexOf(dataName) - 1]] = null;
      prevRef.focus();
    },
    validatePinInput(val, dataName) {
      if (this.shouldFocusNext(val, dataName)) {
        this.$refs[val].blur();
        const {
          one, two, three, four, five, six,
        } = this;
        if (one && two && three && four && five && six) {
          this.$emit('completed', `${one}${two}${three}${four}${five}${six}`);
        }
      }
    },
  },
};
