export const componentStateMixin = {
  methods: {
    setActiveClass(refId, className, stateClassName) {
      Array.from(document.getElementsByClassName(className)).forEach((el) => {
        el.classList.remove(stateClassName);
      });
      // eslint-disable-next-line no-unused-expressions
      this.$refs[refId].length ? this.$refs[refId][0].$el.classList.add(stateClassName) : this.$refs[refId].$el.classList.add(stateClassName);
    },
  },
};
