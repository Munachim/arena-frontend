export default [
  {
    path: '/profile',
    component: () => import('layouts/main/Profile.vue'),
    children: [
      {
        path: 'bank-account',
        component: () => import('pages/main/BankAccount/BankAccount.vue'),
        name: 'Bank Account',
      },
    ],
  },
];
