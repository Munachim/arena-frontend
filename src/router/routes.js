import mainRoutes from './main';
import userRoutes from './user';
import dashboardRoutes from './dashboard';
import apps from './apps';

const routes = [
  ...mainRoutes,
  ...userRoutes,
  ...dashboardRoutes,
  ...apps,
];

export default routes;
