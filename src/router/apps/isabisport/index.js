import store from '../../../store';

export default [
  {
    path: 'isabisport/auth',
    component: () => import('pages/apps/isabisport/Loginpage/LoginPage.vue'),
    name: 'Isabisport Login',
    props: true,
    beforeEnter: (to, from, next) => {
      if (store().state.auth.isAuthenticated && store().state.audience.audienceProfile) {
        next('/apps/isabisport/game-rules');
      }
      next();
    },
  },
  {
    path: 'isabisport/auth/reset',
    component: () => import('pages/apps/isabisport/AuthPages/ResetPin.vue'),
    name: 'Isabisport Reset',
    props: true,
  },
  {
    path: 'isabisport/auth/:type',
    component: () => import('pages/apps/isabisport/AuthPages/Index.vue'),
    name: 'Isabisport AuthPages',
  },
  {
    path: 'isabisport',
    component: () => import('layouts/main/Dashboard/Isabisport.vue'),
    children: [{
      path: 'game-rules',
      component: () => import('pages/apps/isabisport/Game/RulesAndLeaderboard/RulesAndLeaderboard.vue'),
      name: 'Isabisport GameRules',
      meta: { requiresAuth: true },
      props: true,
    },
    {
      path: 'game-result',
      component: () => import('pages/apps/isabisport/Game/GameResult/GameResult.vue'),
      name: 'Isabisport GameResult',
      meta: { requiresAuth: true },
      props: true,
    },
    ],
  },
  {
    path: 'isabisport',
    component: () => import('layouts/main/Profile.vue'),
    children: [{
      path: 'profile',
      component: () => import('../../../pages/apps/isabisport/Profile/Profile.vue'),
      name: 'Profile',
    }],
  },
];
