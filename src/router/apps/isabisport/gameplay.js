export default [
  {
    path: '/gameplay/isabisport',
    component: () => import('pages/apps/isabisport/Game/GamePlay/GamePlay.vue'),
    name: 'Isabisport Gameplay',
    meta: { requiresAuth: true },
  },
];
