import isabisport from './isabisport';
import isabisportGameplay from './isabisport/gameplay';

export default [
  {
    path: '/apps',
    component: () => import('layouts/main/Index.vue'),
    meta: { requiresAuth: false },
    children: [
      ...isabisport,
    ],
  },
  ...isabisportGameplay,
];
