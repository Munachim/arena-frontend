export default [
  {
    path: '/dashboard',
    component: () => import('layouts/main/Dashboard/Dashboard.vue'),
    meta: { requiresAuth: true },
    children: [
      {
        path: '',
        component: () => import('pages/main/Dashboard/Dashboard.vue'),
        name: 'Dashboard',
      },
      {
        path: 'wallet',
        component: () => import('pages/main/Wallet/Wallet.vue'),
        name: 'My Wallet',
      },
      {
        path: 'instant-rewards',
        component: () => import('pages/main/InstantRewards/InstantRewards.vue'),
        name: 'Instant Reward',
      },
      {
        path: 'leaderboard-games',
        component: () => import('pages/main/LeaderboardGames/LeaderboardGames.vue'),
        name: 'Leaderboard Games',
      },
      {
        path: 'game-result',
        component: () => import('pages/main/GameResult/GameResult.vue'),
        name: 'Game Result',
      },
      {
        path: 'success',
        component: () => import('pages/main/SuccessPage/SuccessPage.vue'),
        name: 'Success Result',
      },
    ],
  },
];
