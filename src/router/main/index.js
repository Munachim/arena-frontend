// import isAuth from '../guard/isAuthenticated';

export default [
  {
    path: '/',
    component: () => import('layouts/main/Index.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/apps/isabisport/Loginpage/LoginPage'),
        name: 'landing-page',
        redirect: 'apps/isabisport/auth',
      },
      {
        path: 'terms-and-conditions',
        component: () => import('pages/main/FullTermsAndConditions/FullTermsAndConditions.vue'),
        name: 't&c-full',
      },
    ],
  },
];
