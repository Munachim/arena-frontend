import store from '../../store/index';

function isAuth(to, from, next) {
  const { auth: { isAuthenticated }, audience: { audience } } = store().state;
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);

  if (requiresAuth) {
    // auth route
    if (!isAuthenticated && from.name !== 'Isabisport AuthPages') {
      return next({
        path: '/',
        query: { redirect: to.fullPath },
      });
    }
    if (isAuthenticated && audience) {
      return next();
    }
    return next();
  }
  return next();
}

export {
  isAuth,
};
