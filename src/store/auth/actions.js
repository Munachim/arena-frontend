import { getItem } from 'src/helpers/localStorage.js';
import { authClient } from '../../config/client.js';

/**
 * resent OTP Pin to user phone
 * @param {*} _
 * @param {String} audienceId audience id
 */
export async function requestOtp(_, phoneNumber) {
  await authClient.post('/send/otp', {
    phone_number: phoneNumber,
  });
}

/**
 * validate OTP
 * @param {ContextObject} { commit }
 * @param {Object} payload
 */
export async function validateOtp({ commit }, { otp, pinId }) {
  try {
    const response = await authClient.post('/verify/otp', {
      otp,
      pin_id: pinId,
    });
    const { data } = response;
    const verifiedStatus = data.data.verified;
    commit('SET_OTP', data);
    commit('SET_VALIDATE_OTP_SUCCESS', true);
    commit('SET_OTP_VERIFICATION_MESSAGE', verifiedStatus);
  } catch (err) {
    const { data } = err.response;
    const verifiedStatus = data.data.verified;
    commit('SET_OTP_VERIFICATION_MESSAGE', verifiedStatus);
    commit('SET_VALIDATE_OTP_SUCCESS', false);
  }
}

/**
 * sign in a user with cognito
 * @param {*} context
 * @param payload
 */
export async function signIn({ commit }, { phoneNumber, pin }) {
  try {
    const response = await authClient.post('/login', {
      phone_number: phoneNumber,
      pin,
    });
    const { data } = response.data;
    commit('AUTH_USER_DETAILS', data);
    commit('SET_LOGIN_USER_SUCCESS', true);
    commit('SET_IS_USER_AUTHENTICATED', true);
    commit('SET_ACCESS_TOKEN', data.token);
    commit('SET_USER_ID', data.user.id);
  } catch (err) {
    const { data } = err.response;
    commit('SET_IS_USER_AUTHENTICATED', false);
    commit('SET_LOGIN_USER_SUCCESS', false);
    commit('SET_LOGIN_USER_ERROR_MESSAGE', data.message);
  }
}

/**
 * logout current user
 * @param context
 * @param payload
 */
export function signOut({ commit }) {
  commit('SIGN_OUT');
}

/**
 * Get the authenticated user attributes from cognito
 * @param context
 * @param payload
 */
export async function signUp({ commit }, phoneNumber) {
  try {
    const response = await authClient.post('/user/authentication', {
      phone_number: phoneNumber,
    });
    const { data } = response;
    const isUserAlreadyRegistered = data.is_registered;
    commit('SET_PHONE_NUMBER', phoneNumber);
    commit('SET_IS_USER_ALREADY_REGISTERED', isUserAlreadyRegistered);
    commit('SET_SIGN_UP_SUCCESS', true);
    commit('SET_SIGN_UP_DETAILS', data.data);
  } catch (err) {
    commit('SET_SIGN_UP_SUCCESS', false);
  }
}
/**
 * Get the authenticated user attributes from cognito
 * @param context
 * @param payload
 */
export async function confirmPinReset({ commit }, payload) {
  try {
    await authClient.post('/complete/pin/reset', payload);
    commit('SET_CONFIRM_RESET_PIN_SUCCESS', true);
  } catch (err) {
    commit('SET_CONFIRM_RESET_PIN_SUCCESS', false);
    if (err.response.data) {
      Object.values(err.response.data).forEach((e) => {
        commit('SET_CONFIRM_RESET_PIN_ERROR', e);
      });
    } else {
      commit('SET_CONFIRM_RESET_PIN_ERROR', err.response.data.message || err.message);
    }
  }
}

/**
 * Reset password of an unauthenticated user
 * @param context
 * @param payload
 */
export async function resetPin({ commit }, payload) {
  try {
    const response = await authClient.post('/reset/pin', payload);
    const { pinId } = response.data.data;
    commit('SET_RESET_PIN_SUCCESS', true);
    commit('SET_PIN_ID', pinId);
  } catch (err) {
    commit('SET_RESET_PIN_SUCCESS', false);
    commit('SET_RESET_PIN_ERROR', err.response.data.message || err.message);
  }
}

/**
 * create pin for new user
 * @param {StoreContext} context
 * @param {Object} payload
 */
export async function createPin({ commit }, pin) {
  try {
    const phoneNumber = getItem('phoneNumber');
    await authClient.post('/create/pin', {
      pin,
      phone_number: phoneNumber,
      type: 'isabisport',
      campaign_id: process.env.CAMPAIGN_ID,
    });
    commit('SET_CREATE_PIN_SUCCESS', true);
  } catch (err) {
    commit('SET_CREATE_PIN_SUCCESS', false);
    commit('SET_CREATE_PIN_ERROR', err.response.data.message || err.message);
  }
}
export async function verifyPin({ commit }, pin) {
  try {
    const response = await authClient.get(`/check/pin?pin=${pin}`);
    const pinIsCorrect = response.data.is_correct;
    commit('SET_VERIFY_PIN_SUCCESS', true);
    commit('SET_PIN_IS_CORRECT', pinIsCorrect);
  } catch (err) {
    // the catch block gets the 500 server error response here
    let pinIsCorrect;
    if (err.response.data) pinIsCorrect = err.response.data.is_correct;
    commit('SET_VERIFY_PIN_SUCCESS', false);
    commit('SET_PIN_IS_CORRECT', pinIsCorrect);
    // if there is no response data from  (due to network error etc.)
    if (!err.response.data) {
      commit('SET_VERIFY_PIN_ERROR', err.response.data.message || err.message);
    }
  }
  // TODO: create pin for new user
}
/**
 * Set Reset Pin OTP
 * @param {StoreContext} context
 * @param {Object} payload
 */
export function setResetPinOtp({ commit }, payload) {
  commit('SET_RESET_PIN_OTP', payload);
}
