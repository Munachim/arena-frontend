import { getItem } from 'src/helpers/localStorage';

export default function () {
  return {
    // Authenticated user object
    user: null,
    pinId: '',
    phoneNumber: getItem('phoneNumber') || '',
    isUserAlreadyRegistered: false,
    signupSuccess: false,
    validateOtpSuccess: false,
    otpVerificationMessage: '',
    createPinSuccess: false,
    loginUserSuccess: false,
    resetPinSuccess: false,
    confirmResetPinSuccess: false,
    confirmResetPinError: '',
    resetPinError: '',
    createPinError: '',
    verifyPinError: '',
    pinIsCorrect: false,
    verifyPinSuccess: false,
    userId: getItem('userId') || '',
    authUserDetails: null,
    // Authentication status
    isAuthenticated: getItem('isAuthenticated') || false,
    // User cognito details
    authDetails: {},
    cognitoUser: null,
    token: null,
    otp: '',
    pin: '',
    resetPinOTP: '',
    Username: '',
    signupDetails: {},
  };
}
