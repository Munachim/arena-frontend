import { clearAll } from 'src/helpers/localStorage';

/**
 * set authenticated user
 * @param {Object} state
 * @param {Object} payload
 */
export function SET_AUTH_USER(state, payload) {
  state.user = payload;
}
export function SET_PHONE_NUMBER(state, payload) {
  state.phoneNumber = payload;
}
export function SET_IS_USER_ALREADY_REGISTERED(state, payload) {
  state.isUserAlreadyRegistered = payload;
}
export function SET_PIN_ID(state, payload) {
  state.pinId = payload;
}
export function SET_RESET_PIN_OTP(state, payload) {
  state.resetPinOTP = payload;
}
export function SET_USERNAME(state, payload) {
  state.Username = payload;
}
export function SET_OTP(state, payload) {
  state.otp = payload;
}
export function SET_NEW_PIN(state, payload) {
  state.pin = payload;
}
export function SET_SIGN_UP_SUCCESS(state, payload) {
  state.signupSuccess = payload;
}
export function SET_RESET_PIN_SUCCESS(state, payload) {
  state.resetPinSuccess = payload;
}
export function SET_CONFIRM_RESET_PIN_SUCCESS(state, payload) {
  state.confirmResetPinSuccess = payload;
}
export function SET_CONFIRM_RESET_PIN_ERROR(state, payload) {
  state.confirmResetPinError = payload;
}
export function SET_RESET_PIN_ERROR(state, payload) {
  state.resetPinError = payload;
}
export function SET_SIGN_UP_DETAILS(state, payload) {
  state.signupDetails = payload;
}
export function AUTH_USER_DETAILS(state, payload) {
  state.authUserDetails = payload;
}
export function SET_ACCESS_TOKEN(state, payload) {
  state.token = payload;
  // axios.defaults.headers.common.Authorization = `Bearer ${payload}`;
}
export function SET_USER_ID(state, payload) {
  state.userId = payload;
}
export function SET_LOGIN_USER_SUCCESS(state, payload) {
  state.loginUserSuccess = payload;
}
export function SET_VALIDATE_OTP_SUCCESS(state, payload) {
  state.validateOtpSuccess = payload;
}
export function SET_CREATE_PIN_SUCCESS(state, payload) {
  state.createPinSuccess = payload;
}
export function SET_CREATE_PIN_ERROR(state, payload) {
  state.createPinError = payload;
}
export function SET_VERIFY_PIN_ERROR(state, payload) {
  state.verifyPinError = payload;
}
export function SET_VERIFY_PIN_SUCCESS(state, payload) {
  state.verifyPinSuccess = payload;
}
export function SET_PIN_IS_CORRECT(state, payload) {
  state.pinIsCorrect = payload;
}
export function SET_LOGIN_USER_ERROR_MESSAGE(state, payload) {
  state.loginUserErrorMessage = payload;
}
export function SET_OTP_VERIFICATION_MESSAGE(state, verifiedStatus) {
  let message = '';
  if (verifiedStatus) {
    message = 'Otp verified successfully';
  }
  if (verifiedStatus === 'Expired' || !verifiedStatus) {
    message = 'Invalid pin';
  }
  state.otpVerificationMessage = message;
}

/**
 * Change isAuthenticated status to true
 * @param state
 * @param payload
 * @constructor
 */
export function SET_IS_USER_AUTHENTICATED(state, payload = true) {
  state.isAuthenticated = payload;
}

/**
 * SET COGNITO USER ATTRIBUTE
 * @param state
 * @param attributes
 * @constructor
 */
export function SET_ATTRIBUTES(state, attributes) {
  state.userAttribute = attributes;
  state.username = state.attributes.filter((Obj) => Obj.Name === 'phone')[0].Value;
}

/**
 * SIgn user out
 * @param state
 * @param attributes
 * @constructor
 */
export function SIGN_OUT(state) {
  state.isAuthenticated = false;
  clearAll();
  window.location.reload();
}
