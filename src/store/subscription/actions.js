import axios from 'axios';

const campaignBaseUrl = process.env.CAMPAIGN_BASE_URL;
const campaignId = process.env.CAMPAIGN_ID;

export function initiateIsabisportCharge({ commit }, payload) {
  return axios.post(`${campaignBaseUrl}/campaigns/${campaignId}/subscriptions/isabisport/initialize-charge`, payload)
    .then((res) => {
      const { data } = res.data;
      commit('SET_CURRENT_TRANSACTION', data.response);
      return data;
    });
}

export function getChargeStatus(_, payload) {
  return axios.post(`${campaignBaseUrl}/campaigns/${campaignId}/subscriptions/isabisport/charge-status`, payload)
    .then((res) => res);
}

export function consumeGameplay(_, audienceId) {
  return axios.patch(`${campaignBaseUrl}/campaigns/${campaignId}/subscriptions/audiences/${audienceId}`)
    .then((res) => res);
}
