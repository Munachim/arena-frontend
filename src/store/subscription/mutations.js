/**
 * update terms & condition state
 * @param {Object} state
 * @param {Boolean} payload value to set
 */
export function SET_CURRENT_TRANSACTION(state, payload) {
  state.currentTransaction = payload;
}
