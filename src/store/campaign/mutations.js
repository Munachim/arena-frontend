/**
 * set campaign details
 * @param {Object} state
 * @param {Object} payload
 */
export function SET_CAMPAIGN_DETAILS(state, payload) {
  state.campaignDetails = payload;
}
export function SET_PARTICIPANTS_COUNT(state, payload) {
  state.participants = payload;
}
export function SET_ALL_CAMPAIGNS(state, payload) {
  state.campaigns = payload;
}
export function SET_ALL_REWARDS(state, payload) {
  state.rewards = payload;
}
export function SET_AD_BREAKERS(state, payload) {
  state.adBreakers = payload;
}
export function SET_PLANS(state, payload) {
  state.plans = payload;
}
