import axios from 'axios';
import { client, authClient } from '../../config/client.js';

const campaignBaseUrl = process.env.CAMPAIGN_BASE_URL;
// const testBMBaseUrl = process.env.BMGAMES_BASE_URL;

export function getCampaignDetails({ commit }, campaignId) {
  return client.get(`${campaignBaseUrl}/campaigns/${campaignId}`)
    .then((response) => {
      const { data } = response.data;
      commit('SET_CAMPAIGN_DETAILS', data);
      commit('SET_AD_BREAKERS', data.ad_breakers);
      commit('SET_PLANS', data.subscription_plans);
    });
}
/**
 * get active campaigns
 * @param {*}
 */
export function getActivities({ commit }) {
  return authClient.get('campaigns/active-campaigns')
    .then((response) => {
      const { data } = response;
      commit('SET_ALL_CAMPAIGNS', data);
    });
}
/**
 * get rewards
 * @param {*}
 */
export function getRewards({ commit }, payload) {
  const { CAMPAIGN_ID, IS_MULTIPLE_VOUCER, IS_DAILY_LIMITED } = payload;
  return client.get(`/campaign/${CAMPAIGN_ID}/all-rewards?type=null&is_multiple_vouchers=${IS_MULTIPLE_VOUCER}&is_daily_limited=${IS_DAILY_LIMITED}`)
    .then((response) => {
      const { data } = response;
      commit('SET_ALL_REWARDS', data);
    });
}
/**
 * get all subscription plans
 */
export function getSubscriptionPlans() {
  const campaignId = process.env.CAMPAIGN_ID;
  return axios.get(`${campaignBaseUrl}/campaigns/${campaignId}/subscription-plans`)
    .then((response) => {
      const { data } = response;
      return data;
    });
}
