/**
 * get campaign id
 * @param {StateObject} state
 */
export function campaignId(state) {
  return state.campaignDetails.id;
}
