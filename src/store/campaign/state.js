export default function () {
  return {
    campaignDetails: null,
    campaigns: [],
    rewards: [],
    adBreakers: [],
    plans: [],
  };
}
