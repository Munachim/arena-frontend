import axios from 'axios';
import { client, authClient } from '../../config/client.js';

const campaignBaseUrl = process.env.CAMPAIGN_BASE_URL;
const campaignId = process.env.CAMPAIGN_ID;
// const bmGamesBaseUrl = process.env.BMGAMES_BASE_URL;

export function updateProfile({ commit }, payload) {
  return authClient.put('/update', payload)
    .then((response) => {
      const { data } = response.data;
      commit('SET_AUDIENCE_PROFILE', data);
    });
}
export function updateBank({ commit }, payload) {
  return authClient.put('/update/bank', payload)
    .then((response) => {
      const { data } = response.data;
      commit('SET_AUDIENCE_PROFILE', data);
    });
}

export function getAudienceProfile({ commit }) {
  return authClient.get('/validate-token')
    .then((response) => {
      const { data } = response.data;
      commit('SET_AUDIENCE_PROFILE', data);
    });
}

/**
 * save audience operator
 * @param {ContextObject} context
 * @param {Object} payload
 */
export function saveAudienceOperator({ commit }, payload) {
  return client.patch('/v1/audience/activity', payload).then((response) => {
    commit('SET_AUDIENCE', response.data);
    return response.data;
  });
}
/**
 * check if the requested username is available
 * @param {*} param0
 * @param {String} username
 */
export function checkIfUsernameAvailable({ commit }, username) {
  if (username) {
    return authClient.get(`/check/username?username=${username}`)
      .then((response) => {
        if (response.data.message) {
          commit('SET_USERNAME_TAKEN', false);
        }
        return response.data;
      });
  }
  return false;
}
/**
 * check if the requested email is available
 * @param {*} param0
 * @param {String} username
 */
export function checkIfEmailAvailable({ commit }, email) {
  if (email) {
    return authClient.get(`/validate-email?email=${email}`)
      .then((response) => {
        if (response.data.message) {
          commit('SET_EMAIL_TAKEN', false);
        }
        return response.data;
      });
  }
  return false;
}
/**
 * Award Free gameplay to registering user
 * @param {*} _
 * @param {String} audienceId
 */
export function awardFreeGameplay(_, audienceId) {
  return axios.post(`${campaignBaseUrl}/campaigns/${campaignId}/subscriptions/freegameplays`, {
    campaign_id: campaignId,
    audience_id: audienceId,
  })
    .then((res) => res);
}

/**
 * Get avaialble gameplay for an audience
 * @param {*} _
 * @param {*} payload
 */
export function getAvailableGamePlay({ commit }, userId) {
  return axios.get(`${campaignBaseUrl}/campaigns/${campaignId}/subscriptions/audiences/${userId}`)
    .then((response) => {
      const { data } = response.data;
      commit('SET_AVAILABLE_GAMEPLAY', data);
      return response.data;
    });
}

export function generateReferralCode(_, audienceId) {
  return axios.post(`${campaignBaseUrl}/campaigns/${campaignId}/referrals`, { referrer_id: audienceId })
    .then((response) => response.data);
}

export function getReferralCode({ commit }, audienceId) {
  return axios.get(`${campaignBaseUrl}/campaigns/${campaignId}/referrals/${audienceId}/referrer`)
    .then((response) => {
      commit('SET_REFERRAL_DETAILS', response.data.data);
      return response.data;
    });
}

export function getAudienceSummary({ commit }, audienceId) {
  return axios.get(`${campaignBaseUrl}/campaigns/${campaignId}/audiences/${audienceId}/stats/lastest-game-play`)
    .then((response) => {
      const { data } = response.data;
      commit('SET_AUDIENCE_SUMMARY', data);
      return data;
    })
    .catch((err) => {
      throw err;
    });
}
export function getGamePlaySummary({ commit }, gameplayId) {
  return axios.get(`${campaignBaseUrl}/campaigns/${campaignId}/game-plays/${gameplayId}/summary`)
    .then((response) => {
      const { data } = response.data;
      commit('SET_AUDIENCE_SUMMARY', data);
      return data;
    })
    .catch((err) => {
      throw err;
    });
}
