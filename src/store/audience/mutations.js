export function SET_AUDIENCE(state, payload) {
  state.audience = payload;
}

export function SET_AUDIENCE_PROFILE(state, payload) {
  state.audienceProfile = payload;
}

export function SET_CHANGE_PIN_ACTIVE(state, payload) {
  state.changePinActive = payload;
}

export function SET_USERNAME_TAKEN(state, payload) {
  state.userNameTaken = payload;
}
export function SET_EMAIL_TAKEN(state, payload) {
  state.emailTaken = payload;
}

export function SET_REFERRAL_DETAILS(state, payload) {
  state.referralDetails = payload;
}

export function SET_AUDIENCE_SUMMARY(state, payload) {
  state.gameSummary = payload;
}

export function SET_AVAILABLE_GAMEPLAY(state, payload) {
  state.availableGameplay = payload;
}
export function SET_SUB_MODAL(state, payload) {
  state.subscriptionModalActive = payload;
}
