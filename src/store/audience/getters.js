export function user(state) {
  return state.user;
}

export function notificationCount(state) {
  return state.notifications.length;
}
