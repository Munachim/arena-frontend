import { getObject } from '../../helpers/localStorage';

export default function () {
  return {
    audienceProfile: getObject('audienceProfile') || {
      account_number: '',
      bank_code: '',
      bank_name: '',
      created_at: '',
      email: '',
      first_name: '',
      id: '',
      last_name: '',
      phone_number: '',
      status: '',
      username: '',
    },
    changePinActive: false,
    userNameTaken: false,
    emailTaken: false,
    notifications: [],
    referralDetails: null,
    gameSummary: null,
    availableGameplay: 0,
    subscriptionModalActive: false,
  };
}
