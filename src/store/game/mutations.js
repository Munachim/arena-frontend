/**
 * set leaderboard
 * @param {Object} state
 * @param {Object} payload
 */
export function SET_LEADERBOARD(state, payload) {
  state.leaderboard = payload;
  state.totalParticipants = payload.length;
  state.audienceScore = payload.find((x) => x.audience_id === this.state.audience.audienceProfile.id) || {};
}

export function INCREMENT_PLAY_COUNT(state) {
  // eslint-disable-next-line no-plusplus
  state.playCount++;
}
export function SET_GAMEPLAY_ID(state, payload) {
  state.gamePlayId = payload;
  localStorage.setItem('gamePlayId', JSON.stringify(payload));
}
export function SET_GAME(state, payload) {
  state.game = payload;
}
export function SET_GAME_ID(state, payload) {
  state.gameId = payload;
}
export function SET_REWARD(state, payload) {
  state.reward = payload;
}
export function SET_REWARD_ID(state, payload) {
  state.rewardId = payload;
}
export function SET_GAME_QUESTION(state, payload) {
  state.question = payload.data.question;
  state.gameplayQuestionDetails = payload.data;
}
export function UPDATE_NO_GAME_QUESTION(state, payload) {
  state.totalQuestionsAnswered = payload;
}
export function INCREMENT_TOTAL_CORRECT(state) {
  // eslint-disable-next-line no-plusplus
  state.totalCorrectAnswers++;
}
export function UPDATE_TOTAL_POINTS(state, payload) {
  // eslint-disable-next-line no-plusplus
  state.totalPointsEarned += payload;
}
export function SET_COUNTRIES(state, payload) {
  state.countries = payload;
}
