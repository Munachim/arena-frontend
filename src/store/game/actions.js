import axios from 'axios';
import { client } from '../../config/client.js';

const bmGamesBaseURL = process.env.CAMPAIGN_BASE_URL;
const locationServiceURL = process.env.LOCATION_BASE_URL;

/**
 * fetch game by ID
 * @param {ContextObject} context
 * @param {String} gameId
 */
export function fetchGameById(context, gameId) {
  return client.get(`/game/${gameId}`)
    .then((response) => {
      context.commit('SET_GAME', response.data);
      context.commit('SET_GAME_ID', response.data.id);
    });
}
/**
 * Get and randomize reward to select
 * @param {ContextObject} context
 * @param {Object} payload
 */
export function getRewards(context, payload) {
  return client.get(`/campaign/${payload.campaignId}/rewards?type=${payload.rewardType}&is_multiple_vouchers=${payload.isMultipleVoucher}&is_daily_limited=${payload.isDailyLimited}`)
    .then((response) => {
      const rewardType = response.data[Math.floor(Math.random() * response.data.length)];

      context.commit('SET_REWARD', rewardType);
      context.commit('SET_REWARD_ID', rewardType.id);
    });
}

/**
 * get normal question
 * @param {ContextObject} context
 * @param {Object} payload
 */
export function getQuestion(context, payload) {
  return client.get(`/campaign/${payload.campaignId}/question?audience_id=${payload.audienceId}&type=${payload.questionType}`)
    .then((response) => response.data);
}
/**
 * get game question
 * @param {ContextObject} context
 * @param {Object} payload
 */
export function getGameQuestion(context, payload) {
  return client.get(`/campaign/${payload.campaignId}/quiz-question?audience_id=${payload.audienceId}`)
    .then((response) => {
      context.commit('SET_GAME_QUESTION', response.data);
      return response.data;
    });
}
/**
 * get subscription game question
 * @param {ContextObject} param0
 * @param {Object} payload
 */
export function getSubscriptionQuestion({ commit }, payload) {
  return client.get(`/subscription-question?campaign_id=${payload.campaignId}&audience_id=${payload.audienceId}`)
    .then((response) => {
      commit('SET_GAME_QUESTION', response.data);
      return response.data.data.data.data;
    });
}
/**
 * answer game question
 * @param {ContextObject} context
 * @param {Object} payload
 */
export function answerGameQuestion(context, payload) {
  return client.get(`/question/${payload.questionId}/answer?campaign_id=${payload.campaignId}&audience_id=${payload.audienceId}&choice_id=${payload.optionId}&is_leaderboard=1&point=${payload.points}&campaign_game_id=${payload.questionId}&duration=${payload.duration}`, {
    headers: {
      'x-api-key': process.env.AUTH_TOKEN,
    },
  });
}
/**
 * answer subscription question
 * @param {ContextObject} context
 * @param {Object} payload
 */
export function answerSubscriptionGameQuestion(context, payload) {
  return client.post('/subscription-question/',
    {
      campaign_id: payload.campaignId,
      audience_id: payload.audienceId,
      duration: payload.duration,
      choice_id: payload.optionId,
      question_id: payload.questionId,
    },
    {
      headers: {
        'x-api-key': process.env.AUTH_TOKEN,
      },
    });
}

export function getGameLeaderboard({ commit }, payload) {
  return client.get(`/campaign/${payload.campaignId}/leaderboard?audience_id=${payload.audienceId}`)
    .then((response) => {
      commit('SET_LEADERBOARD', response.data);
    });
}

export function getMainLeaderboard({ commit }, type = 'daily') {
  return axios.get(`${bmGamesBaseURL}/campaigns/${process.env.CAMPAIGN_ID}/leaderboards/${type}`)
    .then((response) => {
      commit('SET_LEADERBOARD', response.data.data);
    });
}
/**
 * start a gameplay
 * @param {ContextObject} { commit }
 * @param {String} audienceId
 * @returns
 */
export function startGamePlay({ commit }, { userId, referrerId }) {
  const payload = { };
  if (referrerId) payload.referrer_id = referrerId;

  return axios.post(`${bmGamesBaseURL}/campaigns/${process.env.CAMPAIGN_ID}/game-plays/${userId}/start`)
    .then((response) => {
      commit('SET_GAMEPLAY_ID', response.data.data);
      return response.data;
    });
}
/**
 * get gameplay question or ad
 * @param {ContextObject} { commit }
 * @param {Object} payload
 * @returns
 */
export function getGameplayQuestionOrAd({ commit }, payload) {
  return axios.get(`${bmGamesBaseURL}/campaigns/${process.env.CAMPAIGN_ID}/game-plays/${payload.gamePlayId}/question-ad`)
    .then((response) => {
      commit('SET_GAME_QUESTION', response.data);
      return response.data;
    });
}
/**
 * answer gameplay question
 * @param {*} _
 * @param {Object} payload
 */
export function answerGameplayQuestion(_, payload) {
  return axios.post(`${bmGamesBaseURL}/campaigns/${process.env.CAMPAIGN_ID}/game-plays/${payload.gamePlayId}/question/${payload.questionId}/answer`, {
    duration: payload.duration,
    choice_id: payload.optionId,
    audience_id: payload.audienceId,
  })
    .then((res) => res.data);
}

export function getCountries({ commit }) {
  return axios.get(`${locationServiceURL}/countries`)
    .then((res) => {
      commit('SET_COUNTRIES', res.data.data);
      return res.data;
    });
}
/**
 * log end of game
 *
 * @export
 * @param {*} { commit }
 * @param {*} gamePlayedId
 * @returns Object
 */
export function endGameServer(_, payload) {
  return client.post(`/update-play/${payload.gamePlayId}`, {
    is_completed: true,
    final_score: payload.finalScore,
  })
    .then((response) => response.data);
}
