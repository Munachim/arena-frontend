export default function () {
  return {
    leaderboard: [],
    audienceScore: null,
    totalParticipants: 0,
    totalCorrectAnswers: 0,
    totalQuestionsAnswered: 0,
    question: null,
    totalPointsEarned: 0,
    gamePlayId: JSON.parse(localStorage.getItem('gamePlayId')) || null,
    gameplayQuestionDetails: null,
    countries: [],
  };
}
