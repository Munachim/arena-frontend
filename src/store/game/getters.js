/**
 * get leaderboard
 * @param {StateObject} state
 */
export function leaderboard(state) {
  return state.leaderboard;
}

export function gamePlayId(state) {
  return state.gamePlayId?.game_play_id;
}
