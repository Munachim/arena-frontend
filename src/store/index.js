import Vue from 'vue';
import Vuex from 'vuex';
// import createPersistedState from 'vuex-persistedstate';
// import SecureLS from 'secure-ls';

import saveStatePlugin from 'src/helpers/store';
import setup from './setup';
import auth from './auth';
import campaign from './campaign';
import audience from './audience';
import bank from './bank';
import wallet from './wallet';
import game from './game';
import subscription from './subscription';
import referral from './referral';

Vue.use(Vuex);

// const ls = new SecureLS({ isCompression: false });
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    plugins: [saveStatePlugin],
    modules: {
      setup,
      auth,
      bank,
      campaign,
      audience,
      wallet,
      game,
      referral,
      subscription,
    },
    // plugins: [createPersistedState({
    //   storage: {
    //     getItem: (key) => ls.get(key),
    //     setItem: (key, value) => ls.set(key, value),
    //     removeItem: (key) => ls.remove(key),
    //   },
    // })],
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING,
  });

  return Store;
}
