import client from '../../config/client.wallet';

/**
 * Create new audience / Sign In
 *
 * @export
 * @param {*} { commit }
 * @param {*} payload
 * @returns
 */
export function createWallet({ commit }, userId) {
  return client.post('/wallets', { user_id: userId })
    .then((response) => {
      const { data } = response.data;
      commit('SET_AUDIENCE_WALLET', data);
    });
}

export function verifyTopUp(_, payload) {
  return client.post('/wallets/transactions/topup', payload)
    .then((response) => response);
}

export function getUserWallet({ commit }, userId) {
  return client.get(`/wallets/user/${userId}`)
    .then((response) => {
      const { data } = response.data;
      commit('SET_AUDIENCE_WALLET', data);
    });
}

export function getChannelList({ commit }) {
  return client.get('/channels')
    .then((response) => {
      const { data } = response.data;
      commit('SET_CHANNEL_LIST', data);
    });
}

export function addCash(_, payload) {
  return client.post('/wallets/transactions/initializeCharge', payload)
    .then((response) => {
      const { data } = response.data;
      return data;
    });
}
