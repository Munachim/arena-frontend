/**
 * set bank list
 * @param {StateObject} state
 * @param {Object} payload
 */
export function SET_AUDIENCE_WALLET(state, payload) {
  state.wallet = payload;
}

export function SET_CHANNEL_LIST(state, payload) {
  state.channels = payload;
}
