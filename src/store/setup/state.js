export default function () {
  return {
    termsAndCondition: true,
    termsAndConditionActive: true,
    selectUserTypeActive: false,
    isNewUser: true,
    hasSkippedUsername: false,
    requestUsernameModal: false,
  };
}
