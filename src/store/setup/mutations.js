/**
 * update terms & condition state
 * @param {Object} state
 * @param {Boolean} payload value to set
 */
export function SET_TERMS_CONDITION(state, payload) {
  state.termsAndCondition = payload;
}
/**
 * set the terms and condition active status
 * @param {Object} state
 * @param {Boolean} payload
 */
export function SET_TERMS_ACTIVE_STATE(state, payload) {
  state.termsAndConditionActive = payload;
}
/**
 * set select user type status
 * @param {Object} state
 * @param {Boolean} payload
 */
export function SET_SELECT_USER_STATE(state, payload) {
  state.selectUserTypeActive = payload;
}
/**
 * set if user is new
 * @param {Object} state
 * @param {Boolean} payload
 */
export function SET_USER_TYPE(state, payload) {
  state.isNewUser = payload;
}

export function SET_SKIP_USERNAME(state, payload) {
  state.hasSkippedUsername = payload;
}

export function SET_REQUEST_USERNAME_MODAL(state, payload) {
  state.requestUsernameModal = payload;
}
