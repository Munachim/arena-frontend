import { getObject } from '../../helpers/localStorage';

export default function () {
  return {
    referral: getObject('referral') || null,
  };
}
