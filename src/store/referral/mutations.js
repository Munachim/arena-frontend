import { saveItem } from '../../helpers/localStorage';
/**
 * set audience
 * @param {Object} state
 * @param {Object} payload
 */
export function SET_REFERRAL(state, payload) {
  state.referral = payload;
  saveItem('referral', payload);
}
