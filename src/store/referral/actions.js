import axios from 'axios';

const campaignBaseUrl = process.env.CAMPAIGN_BASE_URL;
const campaignId = process.env.CAMPAIGN_ID;

export function generateReferral({ commit }, audienceId) {
  return axios.post(`${campaignBaseUrl}/campaigns/${campaignId}/referrals`, {
    referrer_id: audienceId,
  })
    .then((response) => {
      const { data } = response.data;
      commit('SET_REFERRAL', data);
      return data;
    })
    .catch((err) => {
      throw err;
    });
}

export function getReferralCode({ commit }, audienceId) {
  return axios.get(`${campaignBaseUrl}/campaigns/${campaignId}/referrals/${audienceId}/referrer`)
    .then((response) => {
      commit('SET_REFERRAL', response.data.data);
      return response.data;
    });
}

export function useReferralCode(_, { referentId, referralCode }) {
  return axios.post(`${campaignBaseUrl}/campaigns/${campaignId}/referrals/activities`, {
    referent_id: referentId,
    referral_code: referralCode,
  }).then((response) => response.data);
}
