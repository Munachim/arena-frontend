/**
 * set bank list
 * @param {StateObject} state
 * @param {Object} payload
 */
export function SET_BANK_LIST(state, payload) {
  state.banks = payload;
}
