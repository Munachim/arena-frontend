// import axios from 'axios';
// import Vue from 'vue';
import { authClient } from '../../config/client.js';

/**
 * Create new audience / Sign In
 *
 * @export
 * @param {*} { commit }
 * @param {*} payload
 * @returns
 */
export function getBankList({ commit }) {
  return authClient.get('/payment/banks')
    .then((response) => {
      let { data } = response.data.data;
      data = data.sort((a, b) => {
        a = a.name.toLowerCase();
        b = b.name.toLowerCase();

        // eslint-disable-next-line no-nested-ternary
        return a < b ? -1 : a > b ? 1 : 0;
      });
      commit('SET_BANK_LIST', data);
    });
}
