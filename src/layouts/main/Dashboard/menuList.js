const mainMenu = [
  {
    icon: '/icons/home.svg',
    label: 'Home',
    path: '/dashboard',
    separator: true,
  },
  {
    icon: '/icons/activities.svg',
    label: 'Activities',
    path: '',
    separator: true,
  },
  {
    icon: '/icons/wallet.svg',
    iconColor: 'primary',
    label: 'Wallet',
    path: '/dashboard/wallet',
    separator: true,
  },
  {
    icon: '/icons/settings.svg',
    iconColor: 'primary',
    label: 'Settings',
    path: '/profile',
    separator: false,
  },
];

const isabiSportMenu = [
  {
    icon: 'settings',
    iconColor: 'primary',
    label: 'My Profile',
    path: '/apps/isabisport/profile',
    separator: false,
  },
];

export {
  isabiSportMenu,
  mainMenu,
};
