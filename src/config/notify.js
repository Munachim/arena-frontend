import { Notify } from 'quasar';

const success = (message = 'Operation Successful.') => Notify.create({ type: 'positive', message, position: 'top' });
const error = (message = 'Error processing request') => Notify.create({ type: 'negative', message, position: 'top' });
const warning = (message = 'Error processing request') => Notify.create({ type: 'warning', message, position: 'top' });

export default {
  success,
  error,
  warning,
};
