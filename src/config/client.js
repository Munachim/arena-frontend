import axios from 'axios';
import { getItem } from 'src/helpers/localStorage';
import successInterceptor from './interceptors/responses/success';
import failureInterceptor from './interceptors/responses/failure';

const axiosObject = {
  baseURL: process.env.BASE_URL,
  timeout: 50000,
};

const authAxiosObject = {
  baseURL: process.env.AUTHENTICATION_BASE_URL,
  timeout: 50000,
};

export const client = axios.create(axiosObject);

export const authClient = axios.create(authAxiosObject);

client.interceptors.response.use(successInterceptor, failureInterceptor);

authClient.interceptors.request.use((config) => {
  const token = getItem('accessToken');

  config.headers.Authorization = `Bearer ${token}`;

  return config;
});

export default client;
